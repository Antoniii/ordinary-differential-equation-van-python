import numpy as np
from scipy. integrate import odeint
import matplotlib.pyplot as plt

 # create function
def f(y, t):
	y1, y2 = y
	#return [y2, - y2 - y1]
	return [5*y1 - 0.1*y1*y2, 0.1*y2*y1 - 6*y2]

t = np.linspace( 0, 6, 100) # vector of time
y0 = [85, 40] # start value # P(0) = 85 and W(0) = 40
w = odeint(f, y0, t) # solve eq.
y1 = w[:,0]
y2 = w[:,1]
# fig = plt.figure(facecolor='white')
# plt.plot(t, y1, '-o', t, y2, '-o', linewidth=2)
# plt.ylabel("P, W")
# plt.xlabel("t")
# plt.grid(True)
# plt.show() # display


#фазовые траектории:

fig2 = plt.figure(facecolor='white')
plt.plot(y1, y2, linewidth=2)
plt.scatter([85], [40], color = 'yellow', marker = 'o', s = 800)
plt.ylabel("W")
plt.xlabel("P")
plt.grid(True)
plt.show()